# Teste Warren
Este projeto é um integrador dos projetos que compoe o ecosistema do teste.
Ultilizo o [Docker](https://www.docker.com/) e o [Git](https://git-scm.com/) submodules para concentrar em um projeto somente todo o ecosistema necessário e poder integrar o frontend e o backend de uma forma mais simples.
Além de também usar de um script para popular o banco com dados iniciais para o teste.

1. Clonar o projeto
```sh
git clone git@bitbucket.org:joaopaulolazari/test-warren.git
```
ou
```sh
git clone https://joaopaulolazari@bitbucket.org/joaopaulolazari/test-warren.git
```
2. Rodar comando para atualizar os submodulos
```sh
git submodule update --init --recursive
```
3. Rodar o comando para subir os containers
```sh
docker-compose up -d --build
```
4. Se tudo rodar da forma esperada, todo o ecosistema do projeto já estará pronto para ser utilizado:
    * acessando [http://localhost:8080/](http://localhost:8080/) para o frontend
    * acessando [http://localhost:3000/api/docs/](http://localhost:3000/api/docs/) para acessar o swagger do backend
    * acessando [http://localhost:8081/](http://localhost:8081/) para acessar as informações do mongoDB
## Documentações

Dentro de cada projeto tem um Readme explicando melhor os detalhes de cada projeto.
Segue os Readme`s:

* [FrontEnd](https://bitbucket.org/joaopaulolazari/front-warren/src/master/)
* [BackEnd](https://bitbucket.org/joaopaulolazari/api-warren/src/master/)